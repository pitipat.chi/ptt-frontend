const Minio = require('minio');

export default new Minio.Client({
  endPoint: 'backend1.philinelabs.net',
  port: 30001,
  useSSL: false,
  accessKey: 'minio',
  secretKey: 'minio123',
});
