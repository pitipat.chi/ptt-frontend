export default function (file, url) {
  return fetch(url, {
    method: 'PUT',
    body: file,
  });
}
