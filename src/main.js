import Vue from 'vue';
import * as io from 'socket.io-client';
import VueSocketIO from 'vue-socket.io';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import store from './store';
import minio from './plugins/minio';

Vue.use(new VueSocketIO({
  debug: true,
  connection: io('backend1.philinelabs.net:30000'),
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_',
  },
}));

Vue.config.productionTip = false;
Vue.prototype.$minio = minio;

new Vue({
  router,
  vuetify,
  store,
  render: (h) => h(App),
}).$mount('#app');
